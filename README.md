# sshconfig-folds

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](CODE_OF_CONDUCT.md)

This vim plugin adds folding to sshconfig files. This is my first vim plugin. As such, I'm sure there are plenty of bugs, please open an issue if you find any!

Thank you to Steve Losh for writing [Learn Vimscript the Hard Way](http://learnvimscriptthehardway.stevelosh.com/).

## Installation

I'm not knowledgeable about installation methods other than Vundle.A

```
Plugin 'voldemortensen/sshconfig-folds'
```

Or copy `after/syntax/sshconfig.vim` to `<Your .vim directory>/after/syntax/sshconfig.vim`.

## Contributing

Feel free to open any pull requests and/or issues and I will be happy to take a look.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md).
By participating in this project you agree to abide by its terms.
