function! s:IndentLevel(lnum)
    return indent(a:lnum) / &shiftwidth
endfunction

function! s:NextNonBlankLine(lnum)
    let numlines = line('$')
    let current = a:lnum + 1

    while current <= numlines
        if getline(current) =~? '\v\S'
            return current
        endif

        let current += 1
    endwhile

    return -2
endfunction

function! s:InHostSection(lnum)
    let numlines = line('$')
    let current = a:lnum

    while current >= 1
        if getline(current) =~? '\v^Host\s.*$'
            return 1
        endif

        let current -= 1
    endwhile

    return -2
endfunction

function! GetSSHConfigFold(lnum)
    if getline(a:lnum) =~? '\v^\s*$'
        return '-1'
    endif

    let b:in_host_section = s:InHostSection(a:lnum)
    let this_indent = s:IndentLevel(a:lnum)
    let next_indent = s:IndentLevel(s:NextNonBlankLine(a:lnum))

    if b:in_host_section && next_indent > 0
        if next_indent == this_indent
            return this_indent
        elseif next_indent < this_indent
            return this_indent
        elseif next_indent > this_indent
            return '>' . next_indent
        endif
    elseif b:in_host_section && next_indent == 0
        let b:in_host_section = 0
        return this_indent
    endif
endfunction

function! GetSSHConfigFoldText()
    let nl = v:foldend - v:foldstart + 1
    let host = substitute(getline(v:foldstart),"^ *","",1)
    let hostname = ""

    let current = v:foldstart + 1
    while current <= v:foldend
        if getline(current) =~? 'hostname'
            let hostname = substitute(getline(current),"^ *[Hh]ost[Nn]ame *","",1)
        endif

        let current += 1
    endwhile

    "let hostname = substitute(getline(v:foldstart+1),"^ *[Hh]ost[Nn]ame *","",1)
    let txt = '+ ' . host . ' (' . hostname . ') : ' . nl . ' lines'
    return txt
endfunction

setlocal foldmethod=expr
setlocal foldexpr=GetSSHConfigFold(v:lnum)
setlocal foldtext=GetSSHConfigFoldText()
